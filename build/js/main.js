/**************************************************

Landing Page - Interoute
Site: https://bitbucket.org/wilsonalberto/lp-interoute

Author: Wilson Alberto

Date: 27 / 04 / 2015

**************************************************/


//==============================================
//
//Global Variables
//
//==============================================
//

//==============================================
//
//Global Functions
//
//==============================================
//	

//==============================================
//
//jQuery Doc. Ready
//
//==============================================
//
	$(function(){
		$('.knowMore').on('click', function(e){
			e.preventDefault();
			
			$('.included').slideToggle();
		});
	});
//== End Jquery d.ready