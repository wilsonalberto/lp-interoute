var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps');
    /*imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant');*/

var paths = {
  scripts: 'build/js/**/*.js',
  sass: 'build/sass/**/*.scss',
  img: 'build/img/**/*',
  html: 'build/index.html'
};


//DEV TASKS
//Compile Sass
gulp.task('dev-build-sass', function() {
  return gulp.src('build/sass/styles.scss')
    .pipe(sass())
    .on('error', function (err) { console.log(err.message); })
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/css'));
});

//Concat Scripts w/ Sourcemaps
gulp.task('dev-build-js', function() {
  // Minify and copy all JavaScript (except vendor scripts)
  // with sourcemaps all the way down
  return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
      .pipe(uglify())
      .pipe(concat('main.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/js'));
});

gulp.task('imgmin', function () {
    return gulp.src(paths.img)
        /*.pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))*/
        .pipe(gulp.dest('dist/img'));
});



//PROD TASKS
//Compile Sass
gulp.task('prepare-css', function() {
  return gulp.src('build/sass/styles.scss')
    .pipe(sass({sourcemap: false, style: 'expanded'}))
    .on('error', function (err) { console.log(err.message); })
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist/css'));
});
//Concat Scripts
gulp.task('prepare-js', function() {
  // Minify and copy all JavaScript (except vendor scripts)
  // with sourcemaps all the way down
  return gulp.src(paths.scripts)
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest('dist/js'));
});



//MISC TASKS
//Add vendor prefixes
gulp.task('add-prefix', function () {
  return gulp.src('dist/css/styles.min.css')
    .pipe(autoprefixer({
      browsers: ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1'],
      cascade: false
    }))
    .pipe(gulp.dest('dist/css'));
});

//Minify CSS
gulp.task('minify-file', function() {
  gulp.src('dist/css/styles.min.css')
    .pipe(minifycss())
    .pipe(gulp.dest('dist/css'));
});

//Copy index.html
gulp.task('copy', function() {
    gulp.src(['build/index.html'])
    .pipe(gulp.dest('dist'));
});

//Watch SASS Changes & Compile them
gulp.task('watch', function() {
  gulp.watch(paths.sass, ['dev-build-sass']);
  gulp.watch(paths.scripts, ['dev-build-js']);
  gulp.watch(paths.img, ['imgmin']);
  gulp.watch(paths.html, ['copy']);
});

//Production task
gulp.task('default', ['prepare-css', 'prepare-js', 'add-prefix', 'imgmin', 'copy']);

//Development task
gulp.task('dev', ['dev-build-sass', 'dev-build-js', 'imgmin' , 'copy', 'watch']);