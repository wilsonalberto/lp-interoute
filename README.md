# LP - Interoute

A landing page for Interoute

# Requirements

- `node.js`

# Instructions

Run `npm install` to install the projects dependencies

To deploy run `gulp` and you'll find the finished keyboard in the `dist` folder

To develop run `gulp dev` and open the `index.html` file inside the `dist` folder.

While `gulp dev` is running, every change you make to your files will automatically be updated and you can refresh your browser.